*** Settings ***
Library     SeleniumLibrary
Library    AutoItLibrary

*** Test Cases ***
TC1 Upload File
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Click Element    xpath=//span[text()='Select PDF file']
    Sleep    2s
#    SeleniumLibrary.Mouse Down    xpath=//a
    Control Focus   Open    ${EMPTY}    Edit1
    Control Set Text    Open    ${EMPTY}    Edit1   "C:${/}AutomationConcepts${/}profile1.pdf"
    Control Click   Open    ${EMPTY}    Button1
    Sleep    5s

TC2 Upload Bulk File
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Click Element    xpath=//span[text()='Select PDF file']
    Sleep    2s
    Control Focus   Open    ${EMPTY}    Edit1
    Control Set Text    Open    ${EMPTY}    Edit1   "C:${/}AutomationConcepts${/}profile1.pdf" "C:${/}AutomationConcepts${/}profile.pdf"
    Control Click   Open    ${EMPTY}    Button1
    Sleep    5s
*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary

Test Teardown   Wait and Close Browser

*** Keywords ***
Wait and Close Browser
    Sleep    5s
    Close Browser

*** Test Cases ***
TC1 Windows Credential Using URL
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    #https://username:password@URL
    Go To    url=https://admin:admin@the-internet.herokuapp.com/basic_auth
#    Sleep    5s
#    [Teardown]  Close Browser

TC2 Windows Credential Using AutoIT
    Open Browser  browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://@the-internet.herokuapp.com/basic_auth
    Sleep    2s
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {TAB}
    AutoItLibrary.Send      admin
    AutoItLibrary.Send      {ENTER}
#    Sleep    5s
#    [Teardown]  Close Browser

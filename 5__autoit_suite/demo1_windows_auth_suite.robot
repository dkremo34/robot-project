*** Settings ***
Library     SeleniumLibrary
Library     AutoItLibrary

#http://the-internet.herokuapp.com/basic_auth
*** Test Cases ***
TC1 Windows Auth Using Url 
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
#    http://Username:Password@SiteURL
    Go To    url=http://admin:admin@the-internet.herokuapp.com/basic_auth
    Sleep    5s
    [Teardown]  Close Browser

TC2 Windows Auth Using AutoIT
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=http://the-internet.herokuapp.com/basic_auth
    Send    admin
    Send    {TAB}
    Send    admin
    Send    {ENTER}
    Sleep    5s
    [Teardown]  Close Browser


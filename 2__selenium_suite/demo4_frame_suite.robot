*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/
    Select Frame    xpath=//frame[@name='login_page']
    #enter userid as test123
    Input Text    name=fldLoginUserId    test123
    #click on continue
    Click Element    link=CONTINUE
    Unselect Frame
    
*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Launch Browser
    [Arguments]     ${browser_name}     ${url}
    Open Browser    browser=${browser_name}
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=${url}



*** Test Cases ***
TC1
    Launch Browser  edge    http://demo.openemr.io/b/openemr/
    Input Text    css=#authUser    admin
    Input Password    css=#clearPass    pass
    Select From List By Label    css=select[name='languageChoice']      English (Indian)
    Click Element    css=#login-button
    Click Element    xpath=//div[text()='Patient']
    Click Element    xpath=//div[text()='New/Search']
    Select Frame    xpath=//iframe[@name='pat']
    Input Text    id=form_fname    bala
    Input Text    id=form_lname    dina
    Input Text    id=form_DOB    2023-05-23
    #select gender
    Select From List By Label    id=form_sex    Male
    Click Element    id=create
    Unselect Frame
    Select Frame    xpath=//iframe[@id='modalframe']
    Click Element    xpath=//button[normalize-space()='Confirm Create New Patient']
    Unselect Frame
    ${actual_alert_text}    Handle Alert    action=ACCEPT   timeout=20s
    Run Keyword And Ignore Error    Click Element    xpath=//div[@class='closeDlgIframe']
    Select Frame    xpath=//iframe[@name='pat']
    Element Should Contain    xpath=//span[contains(text(),'Record Dashboard')]    bala dina
    Element Text Should Be    xpath=//span[contains(text(),'Record Dashboard')]    Medical Record Dashboard - bala dina
    Unselect Frame
    Should Contain    ${actual_alert_text}    Tobacco   Asserting the alert message
    Close Browser
#    Will start at 11:20 AM
# Please complete OpenEMR task


*** Settings ***
Documentation      This suite file handles all test cases for Sign up
Library     SeleniumLibrary
Library     FakerLibrary
Library      AutoRecorder

*** Test Cases ***
TC1 SignUp
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.salesforce.com/in/form/signup/freetrial-sales/
    ${word}    FakerLibrary.First Name
    Input Text    name=UserFirstName    ${word}
    ${word}    FakerLibrary.Last Name
    Input Text    xpath=//input[contains(@id,'UserLast')]    ${word}
    Input Text    xpath=//input[contains(@id,'UserLast')]    ${word}
    ${email}    FakerLibrary.Company Email
    Input Text    name=UserEmail     ${email}
    Close Browser

TC1
    ${word}   FakerLibrary.Company Email
    Log To Console    ${word}
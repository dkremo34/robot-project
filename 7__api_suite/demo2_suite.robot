*** Settings ***
Library     RequestsLibrary


*** Test Cases ***
TC1 Find Valid Pet Id
    Create Session    alias=petstore    url=https://petstore.swagger.io/v2
    ${response}  GET On Session      alias=petstore  url=/pet/2001    expected_status=200
#    Status Should Be    expected_status=200
    Log   ${response.text}
    Should Contain    ${response.text}    doggie
    Log   ${response.json()}
    Log   ${response.json()}[id]
    Log    ${response.json()}[category]
    Log    ${response.json()}[category][id]
    Log    ${response.json()}[category][name]
    Log    ${response.json()}[name]
    Should Be Equal As Numbers    ${response.json()}[id]    2001
    Should Be Equal As Strings    ${response.json()}[name]    doggie-2001




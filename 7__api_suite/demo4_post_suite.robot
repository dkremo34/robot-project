*** Settings ***
Library     RequestsLibrary
Library    OperatingSystem
Suite Setup     Create Session    alias=petstore    url=https://petstore.swagger.io/v2

*** Keywords ***
Get Auth Token
    Create Session    alias=tokenalias    url=https://petstore.swagger.io/v2
    ${response}  GET On Session      alias=tokenalias  url=/pet/5    expected_status=200
#    Return From Keyword     ${response.json()}[id]
    [Return]    ${response.json()}[id]

*** Test Cases ***
TC1 Add Pet
#    Create Session    alias=petstore    url=https://petstore.swagger.io/v2
    &{header_dic}   Create Dictionary   Content-Type=application/json   Connection=keep-alive
    ${json}     Get Binary File     ${EXECDIR}${/}test_data${/}new_pet.json
    Log To Console    ${json}
    ${response}     POST On Session     alias=petstore   url=/pet
    ...   headers=${header_dic}     data=${json}    expected_status=200
    Log    ${response.json()}

TC2 Delete Valid Pet
    ${token}    Get Auth Token
    &{header_dic}   Create Dictionary   api_key=${token}
    ${response}     DELETE On Session   alias=petstore      url=/pet/2001
       ...  headers=${header_dic}   expected_status=200
    Should Be Equal As Numbers    ${response.json()}[message]    2001
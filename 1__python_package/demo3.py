import requests

url = "https://petstore.swagger.io/v2/pet/2001"

payload = 'username=jack&password=jack123&grant_type=client_credentials&client_id=2234sda'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)
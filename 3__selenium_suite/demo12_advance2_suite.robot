*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome  alias=b1
    Open Browser    browser=edge    alias=b2
    Switch Browser    b1
    Go To    url=http://google.com
    Switch Browser    b2
    Go To    url=http://facebook.com
    Sleep    5s
    Close All Browsers


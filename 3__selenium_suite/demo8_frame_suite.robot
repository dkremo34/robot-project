*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/
    #enter userid as test123
    Select Frame    xpath=//frame[@name='login_page']
    Input Text    name=fldLoginUserId    demo123
    #click on continue
    Click Element    link=CONTINUE
    Unselect Frame
    [Teardown]  Close Browser
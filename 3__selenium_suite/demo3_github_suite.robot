*** Comments ***
Task 1
Navigate to https://github.com/login
Enter username as hello
Enter password as 89hello
Click Sign in
Validate the error message

Task 2
Create a GITLAB (not github) account. As per Ericsson policy, gitlab should be
used for managing the source code

*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://github.com/session
    #Enter username as hello
    Input Text    id=login_field    jack123@gmail.com
    Input Password    id=password    welcome123
    Click Element    name=commit
    #Validate the error message
    Element Should Contain    xpath=//div[@role='alert']    Incorrect username or password
#    Element Should Contain    xpath=//div[contains(text(),'Incorrect')]    Incorrect username or password

*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 Switch Using title
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.db4free.net/
    #click on phpMyAdmin »
#    //b[contains(text(),'phpMy')]
    Click Element    partial link=phpMyAdmin
    #switch to new tab using title
    Switch Window   phpMyAdmin
    Input Text    id=input_username    admin
    Input Text    id=input_password    admin123
    Click Element    id=input_go
    #validate the error message contains "Access denied for user"
    Element Should Contain    id=pma_errors    Access denied for user
    Close Window
    Switch Window   db4free.net - MySQL Database for free
    ${actual_title}     Get Title
    Log To Console   ${actual_title}
    Sleep    10s
    Close Browser

TC2 Switch Using NEW AND MAIN
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.db4free.net/
    Click Element    partial link=phpMyAdmin
    Switch Window   NEW
    Input Text    id=input_username    admin
    Input Text    id=input_password    admin123
    Click Element    id=input_go
    #validate the error message contains "Access denied for user"
    Element Should Contain    id=pma_errors    Access denied for user
    Close Window
    Switch Window   MAIN
    ${actual_title}     Get Title
    Log To Console   ${actual_title}
    Sleep    10s
    Close Browser
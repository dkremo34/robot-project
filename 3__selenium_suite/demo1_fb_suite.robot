*** Settings ***
Library     SeleniumLibrary
Library     FakerLibrary
*** Test Cases ***
TC1
    Open Browser    url=https://www.facebook.com/   browser=chrome
    ${actual_title}     Get Title
    Log To Console   ${actual_title}
    ${current_url}      Get Location
    Log To Console   ${current_url}
    ${page_source}      Get Source  
    Log    ${page_source}
    Sleep    5s
    Close Browser

TC2 Login
    Open Browser    url=https://www.facebook.com/   browser=chrome
    ${first_name}    FakerLibrary.First Name
    Input Text    id=email   ${first_name}@gmail.com
    Input Password    id=pass    welcome123
    Click Element    name=login
    #validate the error message
    
TC3 SignUp
    Open Browser    browser=chrome
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.facebook.com/
    #click on create new account
    Click Element    link=Create new account
    #enter firstname as john
    Input Text    name=firstname    jack
    Sleep    15s
*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://login.salesforce.com/?locale=in
    #click on try free
    Click Element    id=signup_link
    #enter firstname as john
    Input Text    name=UserFirstName    john
    #enter lastname as wick
    Input Text    name=UserLastName    wick
    #enter emailid as john@gmail.com
    Input Text    name=UserEmail    john@gmail.com
    #select job title as IT Manager
    Select From List By Label    name=UserTitle     IT Manager
    #enter company name as Ericsson
    Input Text    name=CompanyName    CCCS
    #Do not fill the phone
    #Select country as United Kingdom
    Select From List By Label    name=CompanyCountry    United Kingdom
    #select employee as 2000+ employees
    Select From List By Value    name=CompanyEmployees  2500
    #click on checkbox
    Click Element    xpath=//div[@class='checkbox-ui']
    #click on start my trial
    Click Element    xpath=//button[text()='start my free trial']
    #validate the error message shown - Enter a valid phone number
    Element Text Should Be    xpath=//span[contains(@id,'UserPhone')]    Enter a valid phone number
*** Comments ***
Navigate onto https://www.online.citibank.co.in/
Close if any pop up comes
Click on Login
Click on Forgot User ID?
Choose Credit Card
Enter credit card number as 4545 5656 8887 9998
Enter cvv number
Enter date as “14/04/2022”
Click on Proceed
Get the text and print it “Please accept Terms and Conditions”

*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 Forgot UserID Check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    10s
    Go To    url=https://www.online.citibank.co.in/
    #Close if any pop up comes
    Click Element    xpath=//a[@class='newclose']
    Click Element    xpath=//a[@class='newclose2']
    #Click on Login
    Click Element    xpath=//span[text()='Login']
    Switch Window   NEW
    #Click on Forgot User ID?
    Click Element    xpath=//div[contains(text(),'Forgot User')]
    #Choose Credit Card
    Click Element    link=select your product type
    Click Element    link=Credit Card
    #Enter credit card number as 4545 5656 8887 9998
    Input Text    css=#citiCard1    7887
    Input Text    css=#citiCard2    7887
    Input Text    css=input[name='citiCard3']    7887
    Input Text    id=citiCard4    7887
    #Enter cvv number as 787
    Input Text    id=cvvnumber    898
    #Enter date as “14/04/2000”

    #approach 1 - not working 
#   Input Text    id=bill-date-long    14/04/2000

    #approach 2  - should work - just follow the steps used in manual approach
    Click Element    id=bill-date-long
    Select From List By Label    xpath=//select[@data-handler='selectYear']     2000
#    Select From List By Label    xpath=//select[@data-handler='selectMonth']    Apr
    Select From List By Value    xpath=//select[@data-handler='selectMonth']    3
    Click Element    link=14
    #Click on Proceed
    Click Element    css=[value='PROCEED']
    #Get the text and print it “Please accept Terms and Conditions”
    Element Should Contain    xpath=//div[@role='dialog']    Please accept Terms and Conditions

    [Teardown]  Close Browser
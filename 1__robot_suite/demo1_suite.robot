*** Settings ***
Library    DateTime

*** Test Cases ***
TC1
    Log To Console  Balaji Dinakaran


TC2
    Log To Console   message=Welcome to session
    Log To Console    bala

TC3
    ${my_name}  Set Variable    bala
    Log To Console    ${my_name}
    Log   ${my_name}

TC4
    ${radius}=   Set Variable   10
    #write logic to calculate area of circle
    ${result}   Evaluate    3.14*${radius}*${radius}
    Log To Console    ${result}


TC5
    ${current_date}     Get Current Date
    Log To Console    ${current_date}
    Log    ${current_date}
    Log To Console    ${OUTPUT DIR}
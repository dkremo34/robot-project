*** Settings ***
Library    Collections

*** Variables ***
${BROWSER}      firefox
@{COLORS}  red  yellow  green
@{fruits}   kkk     jjjj

*** Test Cases ***
TC1
    Log To Console    ${BROWSER}
#    Should Be Equal    ${BROWSER}    chrome
    Log To Console    ${COLORS}
    Log To Console    ${COLORS}[0]
    ${len}      Get Length    ${COLORS}
    Log To Console    ${len}

TC2
    Log To Console    ${COLORS}

TC3
    Log To Console    ${fruits}
    @{fruits}   Create List   mango     apple      grapes
    Log To Console    ${fruits}
    #append new item kiwi
#    Append To List  ${fruits}   kiwi
    Log To Console   ${fruits}[0]
    #remove item apple from the list
    Remove Values From List    ${fruits}    apple
    #add pineapple at index 0
    Insert Into List    ${fruits}    0    pineapple
    #print the list
    Log To Console   ${fruits}
    #print the size of the list
    Log    ${fruits}
    Log List    ${fruits}
#    Set Global Variable    ${fruits}     ${fruits}
    
TC4
    Log To Console    ${fruits}
    Log List    ${fruits}
*** Settings ***
Library    DateTime


*** Test Cases ***
TC1
    Log To Console      message=Hello Everyone
    Log To Console    Welcome to Robot framework session


TC2
    Log To Console    Balaji Dinakaran
    Log    message=Balaji

TC3
    ${my_name}     Set Variable    bala
    Log To Console    ${my_name}

TC4
    ${radius}=     Set Variable      10
    #calculate area of circle and print the output
    ${result}   Evaluate     3.14*${radius}*${radius}
    Log To Console    ${result}
    Log    ${result}
    Should Be Equal As Numbers    ${result}    314.0

TC5
    ${current_date}     Get Current Date
    Log To Console    ${current_date}



print("balaji")

a=10

b=20.2

c="bala"
d=['red','yellow','blue']
e=('red','green','yellow')

d.append('yellow')
d.remove('red')

f=True

my_details={
    "name":"bala",
    "mobile":878787,
}

print(my_details['mobile'])

print(type(a))

print(type(b))

print(type(c))

print(a)
print(b)
print(a+b)

print(c)
print(c[1])
print(d)
print(d[2])

num=0

if num<0:
    print("negative")
elif num==0:
    print("zero")
else:
    print("positive")

#for
for i in range(1,3):
    print(i)

colors=['red','yellow','blue']

for i in range(0,len(colors)):
    print(colors[i])

#advance for loop
for x in colors:
    print(x)
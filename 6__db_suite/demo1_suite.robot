*** Settings ***
Library     DatabaseLibrary

*** Test Cases ***
TC1
   [Setup]  Connect To Database  dbapiModuleName=pymysql     dbName=dbfree_db
   ...  dbUsername=dbfree_db    dbPassword=12345678     dbHost=db4free.net
   ...  dbPort=3306
   ${row_count}      DatabaseLibrary.Row Count    select * from Products
   Log To Console    ${row_count}
   [Teardown]    Disconnect From Database


*** Settings ***
Library     DatabaseLibrary

Test Setup      Connect To Database  dbapiModuleName=pymysql     dbName=dbfree_db
   ...  dbUsername=dbfree_db    dbPassword=12345678     dbHost=db4free.net
   ...  dbPort=3306

Test Teardown   Disconnect From Database

*** Test Cases ***
TC1
   ${row_count}      DatabaseLibrary.Row Count    select * from Products
   Log To Console    ${row_count}


TC2
   ${row_count}      DatabaseLibrary.Row Count    select product_Id from Products where product_Id>10
   Log To Console    ${row_count}

TC3
    [Setup]     None
    Log To Console    hello
    [Teardown]      None
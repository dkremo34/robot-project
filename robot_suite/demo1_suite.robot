*** Settings ***
Library     DateTime

*** Test Cases ***
TC1
    Log To Console    message=Hello word

TC2
    Log To Console  Hello
    Log    welcome
TC3

    ${my_Var}   Set Variable    dheeraj
    Log To Console    ${my_Var}
TC4
    ${radius}   Set Variable    10
    ${result}   Evaluate    (2*${radius}*3.14)
    Log To Console    ${result}
    Log    ${result}
    
TC5
    ${current_date}     Get Current Date
    Log To Console    ${current_date}
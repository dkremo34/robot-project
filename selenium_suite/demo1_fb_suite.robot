*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
#TC1
    #Open Browser    URL=https://www.facebook.com/   browser=chrome

TC2
    Open Browser    url=https://www.facebook.com/   browser=chrome
    Input Text    id:email    dheeraj12345@gmail.com
    Input Text    id=pass    admin12345
    Click Element    name=login
    #validation

TC3 SignUp
    Open Browser    browser=chrome
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.facebook.com/
    Click Element    link=Create new account
    Sleep    5s
    Input Text    name=firstname    Dheeraj
    Input Text    name=lastname    Patel
    Input Text    name=reg_email__    dheeraj1234@gmail.com

TC4 SignUpSales
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://login.salesforce.com/?locale=in
    Click Element    id=signup_link
    Input Text    name=UserFirstName    John
    Input Text    name=UserLastName    Dew
    Input Text    name=UserEmail    john123@gmail.com
    Select From List By Value    name=UserTitle     IT_Manager_AP
    Input Text    name=CompanyName    Eric
    Select From List By Value    name=CompanyEmployees      9
    #Input Text    name=UserPhone    9999876434
    Select From List By Value    name=CompanyCountry    IN
    #Select Checkbox    id=SubscriptionAgreement
    Click Element    xpath=//div[@class='checkbox-ui']
    Click Element    name=start my free trial
    Element Text Should Be    xpath=//span[contains@id,'UserPhone']    Enter a valid phone number
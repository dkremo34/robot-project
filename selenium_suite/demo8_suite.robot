*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1 Forgot Id check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://netbanking.hdfcbank.com/netbanking/
    Select Frame    xpath=//frame[@name='login_page']
    Input Text    name=fldLoginUserId    demo123
    Click Element    link=CONTINUE

    [Teardown]      Close Browser
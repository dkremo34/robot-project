*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Launch Browser
    Open Browser    browser=edge
    Maximize Browser Window
    Set Selenium Implicit Wait    20s

Launch Browser By Name
    [Arguments]     ${browser_name}
    Open Browser    browser=${browser_name}
    Maximize Browser Window
    Set Selenium Implicit Wait    20s

*** Test Cases ***
TC1 Mouse Over
    Launch Browser
    Go To    url=https://nasscom.in/
    Mouse Over    xpath=//a[text()='Membership']
    #element should be present and visible
    Click Element    xpath=//a[text()='Members Listing']

TC2 Mouse over
    Launch Browser
    Go To    url=https://nasscom.in/
    Mouse Over    xpath=//a[text()='Membership']
    #Mouse over on Become a Member
    Mouse Over    xpath=//a[text()='Become a Member']
    #click on Membership Benefits
    Click Element    xpath=//a[text()='Membership Benefits']
    #click on CLICK TO APPLY ONLINE
    Click Element    xpath=//a[text()='Click to Apply Online']
    Title Should Be    Become a Member | nasscom
    Page Should Contain    Membership Application Form
    Element Text Should Be    xpath=//h1[@class='heading']    Membership Application Form
    Element Text Should Be    //h1[contains(text(),'Form')]     Membership Application Form

TC3 Upload File
    Launch Browser By Name    chrome
*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC4 SignUpSales
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://github.com/session
    Input Text    name=login    aa@gmail.com
    Input Text    id=password    admin123
    Click Element    name=commit
    Element Should Contain  xpath=//div[@role='alert']    Incorrect username or password
    #Element Should Contain  xpath=//div[@contains(text(),'Incorrect']    Incorrect username or password
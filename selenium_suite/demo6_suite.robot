*** Settings ***
Library     OperatingSystem
Library    Collections

*** Test Cases ***
TC1
    Log To Console    C:${/}AutomationConcepts${/}DemoFolder
    Create Directory    path=C:${/}AutomationConcepts${/}DemoFolder
    Directory Should Exist    path=C:${/}AutomationConcepts${/}DemoFolder

TC2
    @{file_names}   List Files In Directory
    ...  path=C:${/}Program Files${/}Windows Media Player
    Log To Console    ${file_names}
    Log To Console    ${file_names}[0]
    #print each filename from the list
    ${size}     Get Length    ${file_names}
    FOR    ${i}    IN RANGE    0    ${size}
        Log    ${file_names}[${i}]
    END
#     @{dir_names}   List Directory
#    ...  path=C:${/}Program Files${/}Windows Media Player
#    Append To List      ${file_names}       ${dir_names}

TC3
    ${count}        Set Variable    10
    ${count}        Set Variable    20
    Log To Console    ${count}
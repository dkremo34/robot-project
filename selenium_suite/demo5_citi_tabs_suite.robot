*** Settings ***
Library    SeleniumLibrary
Library    XML

*** Test Cases ***
TC1 CitiBank Forgot Id check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    Go To    url=https://www.online.citibank.co.in/
   Click Element    xpath=//a[@class='newclose']
   Click Element    xpath=//a[@class='newclose2']
   #Click Element    xpath=//span[@class='txtSign']
   Click Element    xpath=//span[text()='Login']
   Switch Window    New
   Click Element    xpath=//div[contains(text(),'Forgot User')]
   Click Element    link=select your product type
   Click Element    link=Credit Card
   Input Text    name=citiCard1    1234
   # Input Text    css=#citiCard1    1234
   Input Text    name=citiCard2    875
   Input Text    name=citiCard3    1632
   Input Text    name=citiCard4    4522
   Input Text    id=cvvnumber    234
   Click Element    id=bill-date-long
   Select From List By Label    xpath=//select[@data-handler="selectYear"]      2000
   Select From List By Label    xpath=//select[@data-handler="selectMonth"]     Apr
   Click Element    link=14
   Click Element    css=[value='PROCEED']
   Element Should Contain    xpath=//div[@role='dialog']    Please accept Terms and Conditions
   Close Browser



